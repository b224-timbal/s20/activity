// alert("hello");

let number = Number(prompt("Please provide a number:"));
console.log("The number you provided is " + number);

for(let x = number; x >= 0; x--) {
	if(x <= 50) {
		console.log("The current value is at 50. Terminating the loop.");
		break;
	};
	isDivTen = x % 10;
	if(isDivTen === 0) {
		console.log("The number is divisible by 10. Skipping number.");
		continue;
	};
	isDivFive = x % 5;
	if(isDivFive === 0) {
		console.log(x);
	};
};

let word = "supercalifraglisticexpialidocious";
let conWord = "";

for(let i = 0; i < word.length; i++){
	console.log(word);
	if(
		word[i].toLowerCase() == "a" ||
		word[i].toLowerCase() == "e" ||
		word[i].toLowerCase() == "i" ||
		word[i].toLowerCase() == "o" ||
		word[i].toLowerCase() == "u" 
		) {
		continue;
	} else {
		conWord += word[i];
	};
};
console.log(conWord);